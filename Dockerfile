FROM python:3

COPY requirements.txt ./
COPY ./test.py ./

RUN pip install --no-cache-dir -r requirements.txt
CMD [ "python", "-u", "./test.py" ]

