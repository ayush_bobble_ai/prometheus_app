import json
from mykafka import MyKafka

def follow(syslog_file):
    pubsub = MyKafka(["a95e9c1b0419711eabea406ab80264e3-443146122.ap-south-1.elb.amazonaws.com:9092"])
    line = syslog_file
    if not line:
        print("no log!")
    else:
        pubsub.send_page_data(syslog_file, 'test1')
with open('./mylog.txt') as f:
    follow(f.read())