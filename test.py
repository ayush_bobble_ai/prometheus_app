from prometheus_client import Counter, start_http_server

import time
from kafka import KafkaConsumer
import json
import sys

start_http_server(8000)
c = Counter('mycount1', 'test counter')
while(True):
    cons = KafkaConsumer('test1', bootstrap_servers="a95e9c1b0419711eabea406ab80264e3-443146122.ap-south-1.elb.amazonaws.com:9092", api_version=(0, 10, 1))
    for msg in cons:
        entry = msg.value.decode()
        entry = entry[1:-1]
        entries = list(entry.split('\\n'))
        print(entries)
        c.inc(len(entries))
        print(c)
    